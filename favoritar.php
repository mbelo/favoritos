<?php
  require_once("comum.php");
  $id_usuario = filter_input(INPUT_POST,"id_usuario", FILTER_SANITIZE_NUMBER_INT);
  $nota = filter_input(INPUT_POST,"nota", FILTER_SANITIZE_NUMBER_INT);

  $stmt = $pdo->prepare("update favoritos set nota=:nota where id=:id");
  $stmt->bindParam( ":nota", $nota, PDO::PARAM_INT);
  $stmt->bindParam( ":id", $id_usuario, PDO::PARAM_INT);
  $result = $stmt->execute();
  if( $result)
  {
      header("Location: favoritos.php#usuario_" . $id_usuario);
  }
  else {
  }